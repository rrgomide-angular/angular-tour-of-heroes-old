import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Wolverine' },
      { id: 2, name: 'Tempestade' },
      { id: 3, name: 'Lanterna Verde' },
      { id: 4, name: 'Capitão América' },
      { id: 6, name: 'Batman' },
      { id: 7, name: 'Superman' },
      { id: 8, name: 'Mulher Maravilha' },
      { id: 9, name: 'Feiticeira Escarlate' },
      { id: 10, name: 'Groot' },
      { id: 11, name: 'Magneto' }
    ];
    return { heroes };
  }
}
